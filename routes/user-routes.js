const express = require('express');
const router = express.Router();

router.get('/', (req,res)=>{
    res.send('returning all users')
})

router.get('/:bookId', (req,res)=>{
    res.send('Returning an user')
})

router.delete('/:bookId', (req,res)=>{
    res.send('deleting an user')
})

module.exports= router;