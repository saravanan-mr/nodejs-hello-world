install docker
docker version
docker info
docker run hello-world
docker ps
    -- list all active containers
docker ps -a
    -- lists all containers that are also stopped.
docker images
    -- lists local images
docker run -d --name web -p 80:8080 sarav/sarav-nodejs
    -- d = detached.
    -- p = port mapping
docker stop $(docker ps -aq)
    -- q returns only the docker id

create Dockerfile
    -- touch Dockerfile
Add steps inside Dockerfile
    -- copy src files, npm install, expose port, start app
Add .dockerignore file
    -- ignore node_modules
Create image
    -- docker build -t mrsarav/node-hello-app .