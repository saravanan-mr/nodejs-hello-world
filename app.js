const express = require('express');
const app = express();
const homeRoutes = require('./routes/home-routes')
const userRoutes = require('./routes/user-routes')

app.use('/', homeRoutes);
app.use('/user', userRoutes);

app.listen('3000', ()=>{ console.log('listening on 3000')})